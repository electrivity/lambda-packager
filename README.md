#Running

In your lambda directory, invoke

	docker run -e HOST_UID=`id -u` -e HOST_GID=`id -g` -v `pwd`:/src electrivity/lambda-packager
