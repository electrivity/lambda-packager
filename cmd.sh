#!/bin/bash
set -e

su $DEFAULT_USER -c "rm -Rf .env node_modules package-lock.json"
su $DEFAULT_USER -c "npm install --production"
